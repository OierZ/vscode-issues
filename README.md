# VSCode Issues

Esta aplicación lista las incidencias del repositorio de Visual Studio Code.

### Instalación del proyecto

Después de descargar el proyecto hay que lanzar el comando `npm install` para descargar todas las dependencias.

### Servidor entorno de desarrollo

Lanza el comando `ng serve` para que angular cree un servidor local y poder probar la aplicación. La url de la aplicación por defecto será `http://localhost:4200/`.

### Compilar la aplicación

Lanza el comando `ng build` para compilar el proyecto. El resultado de la compilación se crea en la carpeta `dist/`. Para compilar para producción se debe usar el "flag" `--prod`.

### Test unitarios

Lanza el comando `ng test` para realizar los tests unitarios.
