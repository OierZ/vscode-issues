import { SafeHtml } from '@angular/platform-browser';
export interface GithubIssue {
  titulo: string;
  descripcion: SafeHtml;
  autor: string;
  numero: number;
  url: string;
}

export interface GithubRepo {
  issueCount: number;
}
