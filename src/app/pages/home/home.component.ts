import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { GithubIssue, GithubRepo } from './../../model/github.model';
import { GithubService } from './../../services/github.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  issues: GithubIssue[];
  repo: GithubRepo;
  pageSize: number;
  pageSizeOptions: number[];
  showLoading: boolean;
  pageIndex: number;

  constructor(private ghService: GithubService) {
    this.issues = [];
    this.repo = { issueCount: 0 };
    this.pageSizeOptions = [5, 10, 20, 50, 100];
    this.pageSize = 10;
    this.showLoading = true;
    this.pageIndex = 0;
  }

  ngOnInit(): void {
    this.getIssues(this.pageSize, 0);
    this.ghService.getVSCodeIssuesNumber().subscribe((resp: GithubRepo) => {
      this.repo = resp;
    });
  }

  /**
   * Actualiza las variables pageSize y pageIndex y llama a la función getIssues con los nuevos datos
   *
   * @param {PageEvent} ev
   * @memberof HomeComponent
   */
  paginator(ev: PageEvent): void {
    this.pageSize = ev.pageSize;
    this.pageIndex = ev.pageIndex;
    this.getIssues(ev.pageSize, ev.pageIndex);
  }

  /**
   *   Recibe una lista de incidencias del repositorio
   *
   * @param {*} pageSize
   * @param {*} pageIndex
   * @memberof HomeComponent
   */
  getIssues(pageSize: any, pageIndex: any): void {
    this.showLoading = true;
    this.ghService.getVSCodeIssues(pageSize, pageIndex).subscribe(
      (resp: GithubIssue[]) => {
        console.log(resp);
        this.showLoading = false;
        this.issues = resp;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
