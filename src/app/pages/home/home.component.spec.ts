import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PageEvent } from '@angular/material/paginator';
import { By } from '@angular/platform-browser';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let de: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [],
      imports: [HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render h1 tag and show title', () => {
    expect(de.nativeElement.querySelector('h1')).toBeTruthy();
  });

  it(
    'should show loading if issues empty',
    waitForAsync(() => {
      let loading = de.nativeElement.querySelector('.backdrop');
      if (component.showLoading) {
        expect(loading).toBeTruthy();
      } else {
        expect(loading).toBeFalsy();
      }
    })
  );

  it('should render second paginator', () => {
    component.pageSize = 20;
    fixture.detectChanges();
    expect(de.query(By.css('#paginatorBottom'))).toBeTruthy();
  });

  it('shouldnt render second paginator', () => {
    component.pageSize = 10;
    fixture.detectChanges();
    expect(de.query(By.css('#paginatorBottom'))).toBeFalsy();
  });

  it('should update paginator data', () => {
    let ev: PageEvent = new PageEvent();
    ev.length = 4500;
    ev.pageIndex = 3;
    ev.pageSize = 20;
    component.paginator(ev);
    fixture.detectChanges();
    expect(component.pageIndex).toBe(3);
    expect(component.pageSize).toBe(20);
  });
});
