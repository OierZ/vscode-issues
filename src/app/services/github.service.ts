import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { GithubIssue, GithubRepo } from './../model/github.model';

@Injectable({
  providedIn: 'root',
})
export class GithubService {
  baseUrl: string;
  constructor(private http: HttpClient, private _sanitizer: DomSanitizer) {
    this.baseUrl = 'https://api.github.com/repos/microsoft/vscode';
  }

  /**
   *
   * Obtiene la lista de incidencias del repositorio, pasando por parametro los datos de paginación.
   *
   * @param {number} pageSize
   * @param {number} pageIndex
   * @return {Observable<GithubIssue[]>}  {Observable<GithubIssue[]>}
   * @memberof GithubService
   */
  public getVSCodeIssues(
    pageSize: number,
    pageIndex: number
  ): Observable<GithubIssue[]> {
    return this.http
      .get<GithubIssue[]>(
        this.baseUrl +
          '/issues?per_page=' +
          pageSize +
          '&page=' +
          (pageIndex + 1),
        {
          headers: this.getHeaders(),
        }
      )
      .pipe(
        map((data: any) => {
          return data.map((d: any) => {
            let issue: GithubIssue = {
              titulo: d.title,
              descripcion: this._sanitizer.bypassSecurityTrustHtml(d.body),
              autor: d.user.login,
              numero: d.number,
              url: d.html_url,
            };
            return issue;
          });
        }),
        catchError((error) => {
          return throwError('error', error);
        })
      );
  }

  /**
   *  Obtiene los datos del repositorio  del cual devuelve el numero de incidencias.
   *
   * @return {Observable<GithubRepo>}  {Observable<GithubRepo>}
   * @memberof GithubService
   */
  public getVSCodeIssuesNumber(): Observable<GithubRepo> {
    return this.http
      .get(this.baseUrl, {
        headers: this.getHeaders(),
      })
      .pipe(
        map((data: any) => {
          let repo: GithubRepo = {
            issueCount: data.open_issues_count,
          };
          return repo;
        })
      );
  }

  private getHeaders() {
    return { Accept: 'application/vnd.github.v3+json' };
  }
}
