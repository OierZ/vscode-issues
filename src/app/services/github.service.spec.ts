import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { GithubIssue, GithubRepo } from '../model/github.model';
import { GithubService } from './github.service';
import { GithubIssuesMock, GithubRepoMock } from './githubMock';

describe('GithubService', () => {
  let service: GithubService, httpTestingController: HttpTestingController;
  let githubIssuesMock = GithubIssuesMock;
  let githubRepoMock = GithubRepoMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GithubService],
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(GithubService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('baseurl should be correct', () => {
    expect(service.baseUrl).toBe(
      'https://api.github.com/repos/microsoft/vscode'
    );
  });

  it('should retrieve all issues from the API via GET', () => {
    service.getVSCodeIssues(10, 0).subscribe((issues: GithubIssue[]) => {
      expect(issues.length).toBeGreaterThan(0);
      expect(issues[0].titulo).toBe(
        '[Need help] Unable to write to User Settings because ... is not a registered configuration.'
      );
    });

    const request = httpTestingController.expectOne(
      service.baseUrl + '/issues?per_page=10&page=1'
    );

    expect(request.request.method).toBe('GET');

    request.flush(githubIssuesMock);
  });

  it('should retrieve VSCode repo from the API via GET', () => {
    service.getVSCodeIssuesNumber().subscribe((repo: GithubRepo) => {
      expect(repo.issueCount).toEqual(4451);
    });

    const request = httpTestingController.expectOne(service.baseUrl);

    expect(request.request.method).toBe('GET');

    request.flush(githubRepoMock);
  });
});
